/*
Playstation DualSense RGB
Copyright (C) 2021  Jacob Gogichaishvili

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import { parse } from "https://deno.land/std@0.83.0/flags/mod.ts";

// RGB Array Type
type rgbArray = [number, number, number];

// Parse Arguments
const args = parse(Deno.args, {
  alias: { help: "h", freq: "f", jump: "j", debug: "d" },
  boolean: ["help", "debug"],
  default: { freq: 20, jump: 2 },
});

// Display help if requested
if (args.help) {
  console.log(`
Help: command [-h|--help] (RGB LED Folder) [-f|--freq=NUMBER] [-j|--jump=NUMBER]

(RGB LED Folder): The folder in which the RGB light strips are located. Example: /sys/class/leds/playstation::ff:ee:aa:77:66:4f::rgb. To automatically detect this folder, instead supply /sys/class/leds/playstation::*::rgb

-h|--help:         Display help
-f|--freq=NUMBER:  The frequency in miliseconds at which the color changes.
-j|--jump=NUMBER:  How much the hue increases each frequency.

Exit codes:

0: The command executed successfully.
1: Something went wrong on the program's end.
2: Invalid arguments provided.
`);
  Deno.exit(0);
}

// Making sure arguments are valid
if (typeof args._[0] != "string") {
  console.log("Invalid folder provided.");
  Deno.exit(2);
} else if (typeof args.freq != "number") {
  console.log("Invalid frequency provided.");
  Deno.exit(2);
} else if (typeof args.jump != "number") {
  console.log("Invalid hue jump provided.");
  Deno.exit(2);
} else if (args.jump > 360) {
  console.log("Hue jump cannot be higher than 360");
  Deno.exit(2);
}

// Seperate rgbFolder into a seperate letiable for better readability
const rgbFolder = args._[0];

// Open file for writing
const rgbFile = await Deno.open(`${rgbFolder}/multi_intensity`, {
  write: true,
});

/**
 * Converts an HSL color value to RGB. Conversion formula
 * adapted from http://en.wikipedia.org/wiki/HSL_color_space.
 * Assumes h, s, and v are contained in the set [0, 1] and
 * returns r, g, and b in the set [0, 255].
 *
 * @param   number  hue              The hue
 * @param   number  saturation       The saturation
 * @param   number  lightness        The value
 * @return  rgbArray                 The RGB representation
 */
// expected hue range: [0, 360)
// expected saturation range: [0, 1]
// expected lightness range: [0, 1]
function HSLtoRGB(
  hue: number,
  saturation: number,
  lightness: number
): rgbArray {
  // based on algorithm from
  // http://en.wikipedia.org/wiki/HSL_and_HSV#Converting_to_RGB
  if (hue == undefined) {
    return [0, 0, 0];
  }

  var chroma = (1 - Math.abs(2 * lightness - 1)) * saturation;
  var huePrime = hue / 60;
  var secondComponent = chroma * (1 - Math.abs((huePrime % 2) - 1));

  huePrime = Math.floor(huePrime);
  let red = 0;
  let green = 0;
  let blue = 0;

  if (huePrime === 0) {
    red = chroma;
    green = secondComponent;
    blue = 0;
  } else if (huePrime === 1) {
    red = secondComponent;
    green = chroma;
    blue = 0;
  } else if (huePrime === 2) {
    red = 0;
    green = chroma;
    blue = secondComponent;
  } else if (huePrime === 3) {
    red = 0;
    green = secondComponent;
    blue = chroma;
  } else if (huePrime === 4) {
    red = secondComponent;
    green = 0;
    blue = chroma;
  } else if (huePrime === 5) {
    red = chroma;
    green = 0;
    blue = secondComponent;
  }

  var lightnessAdjustment = lightness - chroma / 2;
  red += lightnessAdjustment;
  green += lightnessAdjustment;
  blue += lightnessAdjustment;

  return [
    Math.abs(Math.round(red * 255)),
    Math.abs(Math.round(green * 255)),
    Math.abs(Math.round(blue * 255)),
  ];
}

async function writeData(file: Deno.File, values: rgbArray) {
  const encoder = new TextEncoder();
  await file.seek(0, Deno.SeekMode.Start);
  await file.write(encoder.encode(values.join(" ")));
}

let loopNo = 0;

async function loop() {
  const rgbVal = HSLtoRGB(loopNo, 1, 0.5);
  await writeData(rgbFile, rgbVal);
  if (args.debug) {
    console.log(loopNo, rgbVal);
  }
  loopNo += args.jump;
  if (loopNo > 360) loopNo = 0;
  setTimeout(loop, args.freq);
}

// Copyright Disclaimer

console.log(`Playstation DualSense RGB  Copyright (C) 2021  Jacob Gogichaishvili
This program comes with ABSOLUTELY NO WARRANTY;
This is free software, and you are welcome to redistribute it
under certain conditions;
`);

loop();
