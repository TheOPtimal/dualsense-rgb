# PlayStation DualSense RGB

Allows you to get PC-like RGB on your DualSense.

## Requirements

- DualSense
- A PC running Linux. (Doesn't work in WSL, you could use a VM with USB passthrough)
- Deno
- Terminal

## Usage

`sudo deno run --allow-write https://gitlab.com/TheOPtimal/dualsense-rgb/-/raw/master/main.ts /sys/class/leds/playstation::*::rgb/`

### Help

    Help: command [-h|--help] (RGB LED Folder) [-f|--freq=NUMBER] [-j|--jump=NUMBER]

    (RGB LED Folder): The folder in which the RGB light strips are located. Example: /sys/class/leds/playstation::ff:ee:aa:77:66:4f::rgb. To automatically detect this folder, instead supply /sys/class/leds/playstation::*::rgb

    -h|--help:         Display help
    -f|--freq=NUMBER:  The frequency in miliseconds at which the color changes.
    -j|--jump=NUMBER:  How much the hue increases each frequency.

    Exit codes:

    0: The command executed successfully.
    1: Something went wrong on the program's end.
    2: Invalid arguments provided.
